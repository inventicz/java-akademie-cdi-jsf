package cz.inventi.javakademie.cdi.jsf.lecture.cdi;

/**
 *
 * @author Tomas Poledny <tpoledny at inventi.cz>
 * 
 */
public interface OS {
	
	public void boot();
	
	public boolean login(String password);
	
}
