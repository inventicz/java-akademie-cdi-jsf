package cz.inventi.javakademie.cdi.jsf.lecture.jsf;

import cz.inventi.javakademie.cdi.jsf.lecture.cdi.OS;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @author Tomas Poledny <tpoledny at inventi.cz>
 * 
 */
@Named
@RequestScoped
public class Computer {

	private OS windows;
	
	private String password;
	
	
	public String startWindows(){
		windows.boot();
		if(!windows.login(password)) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Wrong password"));
			return null;
		}
		return "logged-win";
	}
	

	public OS getWindows() {
		return windows;
	}

	public void setWindows(OS windows) {
		this.windows = windows;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
		
}
