package cz.inventi.javakademie.cdi.jsf.lecture.cdi;

/**
 *
 * @author Tomas Poledny <tpoledny at inventi.cz>
 * 
 */
public class WindowsOS implements OS {

	@Override
	public void boot() {
		
	}

	@Override
	public boolean login(String password) {
		return true;
	}

}
